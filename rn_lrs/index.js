/** @format */
import {AppRegistry,Text} from 'react-native';
import App from './src/main/App';

if (Text.defaultProps == null) Text.defaultProps = {};
Text.defaultProps.allowFontScaling = false;
Text.defaultProps.style = { fontFamily: 'Arial' }

AppRegistry.registerComponent('emptyProject', () => App);