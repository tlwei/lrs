import LoginScreen from '../../components/userComponent/loginPage'
import { ApplicationStack } from '../contentNavigator/applicationNavigator/homeNavigator'
import { DashboardTabNavigator } from '../../navigator/mainNavigator/footerNavigation'
import { createSwitchNavigator } from 'react-navigation';

export const AppSwitchNavigator = createSwitchNavigator({
	LoginScreen:LoginScreen,
	WelcomeScreen:DashboardTabNavigator
})