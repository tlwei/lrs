import React, { Component } from 'react';
import Application from './../../../containers/homeContainer';
import Ticked from './../../../components/userAction/ticked';
import Notes from './../../../components/userAction/noteList';
import NotesData from './../../../components/userAction/note';
import Audio from './../../../components/userAction/audio';
import Qrcode from './../../../components/userAction/qrcode';
import {StyleSheet} from 'react-native';
import ApplicationDetails from './../../../components/application/scannedLeads';
import HotLeads from './../../../components/application/hotLead';
import { Icon } from 'native-base';
import { createStackNavigator } from 'react-navigation';

const styles = StyleSheet.create({
	header:{
		marginRight:50,
		textAlign: 'center',
		flexGrow:1,
		alignSelf:'center',
	}
})

/////Application page
export const ApplicationStack = createStackNavigator({
	Application:{
		screen:Application,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:'QR Code',
				headerTitleStyle: {
					textAlign: 'center',
					flexGrow:1,
					alignSelf:'center',
				},
				headerLeft:(
					<Icon style={{paddingLeft:10}} onPress={()=>navigation.navigate('LoginScreen')} name='md-exit' size={30}/>
				),
				headerRight:(
					<Icon style={{paddingRight:10}} onPress={()=>navigation.navigate('Qrcode')} name='md-qr-scanner' size={30}/>
				)
			}
		}
	},
	Detail:{
		screen:ApplicationDetails,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:'Scanned Leads',
				headerTitleStyle: styles.header
			}
		}
	},
	HotLeads:{
		screen:HotLeads,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:'Hot Leads',
				headerTitleStyle: styles.header
			}
		}
	},
	Ticked:{
		screen:Ticked,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:'Select Action',
				headerTitleStyle: styles.header
			}
		}
	},
	Notes:{
		screen:Notes,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:'Notes List',
				headerTitleStyle: styles.header
			}
		}
	},
	NotesData:{
		screen:NotesData,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:'Note',
				headerTitleStyle: styles.header
			}
		}
	},
	Audio:{
		screen:Audio,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:'Audio',
				headerTitleStyle: styles.header
			}
		}
	},
	Qrcode:{
		screen:Qrcode,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:'QRCode Scanner',
				headerTitleStyle: styles.header
			}
		}
	}
})
