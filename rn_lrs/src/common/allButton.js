import React, { Component } from 'react';
import { Button, Text } from 'native-base';
import { StyleSheet } from 'react-native';
import { buttonColor,buttonText } from './../config/constants'

export default class first_page extends Component {
  render() {
    return (
        <Button onPress={this.props.navigateTo} style={styles.button}>
          <Text style={{fontSize:16,color:buttonText}}>{this.props.buttonTitle}</Text>
        </Button>
    );
  }
}

const styles = StyleSheet.create({
  button:{
      width: '95%',
      justifyContent: 'center',
      alignSelf:'center',
      backgroundColor:buttonColor,
      marginTop:25
  }
});