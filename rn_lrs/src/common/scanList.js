import React, { Component } from 'react';
import { Icon,Right,Left, ListItem, Text } from 'native-base';
import AllText from './allText';
export default class ScanList extends Component {
  render() {
    return (
		<ListItem button onPress={this.props.navigateTo}>
            <Left>
              <AllText text={this.props.player}/>
            </Left>
            <Right>
              <Icon name={this.props.icon} />
            </Right>
        </ListItem>
    );
  }
}