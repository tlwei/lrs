import React, { Component } from 'react';
import { Footer, FooterTab } from 'native-base';
import { Image } from 'react-native';
import {footerUrl} from './../config/constants'
export default class FooterImage extends Component {

    render() {
        let footer = {uri:footerUrl}
        return (
            <Footer style={{backgroundColor:'#0000'}}>
                <FooterTab>
                    {/* 1000*140 */}
                    {/* require('./../../appImages/footer.jpg') */}
		            <Image source={footer} style={{ height: 55, flex: 1}}/>
	            </FooterTab>
            </Footer>
        )
    }
}
