import React from 'react';
import { View, StyleSheet,Text,Image } from 'react-native';

export default class Loading extends React.Component {

  render() {
    return (
        <View style={styles.modalBackground}>
          <Text style={{fontSize:20,padding:50}}></Text>
          <Image style={styles.activityIndicatorWrapper} source={require('../../assets/all4dlogo.png')}/>
          <Text style={{fontSize:20,padding:50}}>AppName</Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    activityIndicatorWrapper: {
        backgroundColor: '#00000000',
        height: 150,
        width: 150,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
  });