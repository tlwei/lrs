import React, { Component } from 'react';
import { Text } from 'native-base';
import { StyleSheet } from 'react-native';
import { commonFontSize } from '../config/constants'
export default class first_page extends Component {
  render() {
    return (
        <Text note numberOfLines={1} style={[styles.welcome]}>{this.props.text}</Text>
    );
  }
}

const styles = StyleSheet.create({
  welcome: {
	fontSize: commonFontSize,
	color:'rgb(81, 81, 81)',
	textAlign: 'center',
  }
});