import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';

function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
}
class OfflineNotice extends PureComponent {
  render() {
      return <MiniOfflineSign />;
  }
}
const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',


  },
  offlineText: {
    color: '#fff'
    
  }
});
export default OfflineNotice;
