import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import {reducer} from './../reducers/index'

 export const store = createStore(
	  reducer,
	  //for redux debug use 
	  composeWithDevTools(
		  applyMiddleware(thunk)
	  )
	);
