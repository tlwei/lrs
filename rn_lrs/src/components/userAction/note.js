import React, { Component } from 'react';
import { Container, Header, Content, Textarea, Form } from 'native-base';
import {StyleSheet,Dimensions,Text} from 'react-native';
import AllButton from './../../common/allButton';
export default class TextArea extends Component {
  constructor(props){
    super(props)
    this.state={
      text:this.props.navigation.state.params.note
    }
  }
  saveNote(){
    this.props.navigation.goBack()
  }

  render() {
    return (
      <Container>
        <Content padder>
          <Form>
            <Textarea style={styles.textStyle} 
             rowSpan={10} 
             bordered 
             placeholder='Input note here..' 
             onChangeText={(value) => {
               this.setState({text:value});
             }}
             value={this.state.text}
             />
          </Form>
        </Content>
        <AllButton navigateTo={()=>this.saveNote()} buttonTitle='SAVE' />
        <Text/><Text/><Text/>
      </Container>
    );
  }
}

const styles=StyleSheet.create({
  textStyle:{
    height:Dimensions.get('window').height/1.5,
    fontSize: 20,
	  color:'rgb(81, 81, 81)',
  }
})