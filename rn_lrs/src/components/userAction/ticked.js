import React, { Component } from 'react';
import { Container, Header, Content, ListItem, CheckBox, Text, Body } from 'native-base';
export default class Ticked extends Component {
  constructor(props){
    super(props)
    this.state={
      checked:false
    }
  }

  checked(){
    this.state.checked == false ? this.setState({checked:true}):this.setState({checked:false})
  }
  render() {
    let ticked = this.props.navigation.state.params.ticked
    return (
      <Container>
        <Content>
          {ticked.map((detail,index)=>(
            <ListItem key={index} button onPress={()=>{this.checked()}}>
              <Body>
                <Text>{detail.name}</Text>
              </Body>
              <CheckBox checked={detail.status} color="green"/>
            </ListItem>
          ))}
        </Content>
      </Container>
    );
  }
}