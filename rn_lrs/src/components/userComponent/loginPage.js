import React, { Component } from 'react';
import { Container,Item,Label,Input, Header, Content, Footer, FooterTab, Button, Icon, Text, Form } from 'native-base';
import { TouchableOpacity,Dimensions,Image,TextInput,StyleSheet,View } from 'react-native';
import HeaderImage from './../../common/headerImage'
import FooterImage from './../../common/footerImage'
import LoadingScreen from './../../common/loading'
import AllButton from './../../common/allButton'
import {mainTitle,buttonColor,titleColor,backgroundColor} from './../../config/constants'
export default class first_page extends Component {
	constructor(){
		super()
		this.state={
			loading:false
		}
	}

	pressBtn(){
		this.setState({loading:true})
		setTimeout(()=>{
			this.props.navigation.navigate('WelcomeScreen')
		},1100)
		
	}
  render() {
		let title = mainTitle
    return (
      <Container style={styles.container}>
				<Content>
        <LoadingScreen loading={this.state.loading} />
						{/* 1000 * 750 */}
          	<HeaderImage />
          	<View>
          	  <Text style={styles.welcome}>{title}</Text>
          	</View>
						
						<Form>
							<Item floatingLabel>
          		    <Label>Username</Label>
          		    <Input style={{height:55}}
          		 		onChangeText={value => this.setState({ user: value })}/>
          		</Item>
							<Item floatingLabel>
          		    <Label>Password</Label>
									<Input style={{height:55}}
									 secureTextEntry={true}
          		 		 onChangeText={value => this.setState({ password: value })}
									 onSubmitEditing={()=>console.log('submit')} />
          		</Item>
						</Form>

          	<AllButton navigateTo={()=>this.pressBtn()} buttonTitle='Login' />
					</Content>
					 {/* 1000 * 140 */}
					<FooterImage />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		backgroundColor:backgroundColor,
	  flex: 1,
	},
	welcome: {
		paddingTop:15,
		fontSize: 24,
		fontWeight:'bold',
		color:titleColor,
	  textAlign: 'center',
	}
});