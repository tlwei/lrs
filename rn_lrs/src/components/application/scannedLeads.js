import React, { Component } from 'react';
import { Container,Icon,Right,Left, Header, Content, List, ListItem, Text } from 'native-base';
import ScanList from './../../common/scanList';
import AllText from '../../common/allText';
import {listIcon,userData} from './../../config/constants';
export default class ListDividerExample extends Component {
  render() {
    return (
      <Container>
        <Content>
          <List>
            <ListItem itemDivider>
              <AllText text={'Scanned person: '+ userData.length}/>
            </ListItem>    
            {userData.map((user,index)=>(
              <ScanList 
               key={index} 
               player={user.name+', '+user.position} 
               icon={listIcon} 
               navigateTo={()=>this.props.navigation.navigate('HotLeads',{profile:user})} />
            ))}
          </List>
        </Content>
      </Container>
    );
  }
}