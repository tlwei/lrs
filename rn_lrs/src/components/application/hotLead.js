import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
import { Container,View,Body,Card,CardItem,Button,Header, Content, Thumbnail, Text } from 'native-base';
import AllText from './../../common/allText';
import AllButton from './../../common/allButton';
export default class ThumbnailExample extends Component {
	constructor(props){
		super(props)
		this.state={

		}
	}

	userAction(action){
		let actionData = this.props.navigation.state.params.profile;
		if(action == 'ticked'){
			this.props.navigation.navigate('Ticked',{ticked:actionData.ticked});
		}else if(action == 'note'){
			this.props.navigation.navigate('Notes',{note:actionData.note});
		}else if(action == 'audio'){
			this.props.navigation.navigate('Audio',{audio:actionData.audio});
		}
	}

  render() {
		let user = this.props.navigation.state.params.profile;
		let arr = [user.name,user.delegate,user.position,user.interest]
		let btnFunc = [
			['ticked','I WANT TO ..'],
			['note','ADD/VIEW NOTES'],
			['audio','ADD AUDIO']
		]
    return (
        <Container>
					<Content>
						<View style={styles.container}>
							<Thumbnail source={{uri:user.userPic}} style={{height: 220, width: 220,margin:15}}/>
							<Card style={{width:250}}>
            		<CardItem>
              		<Body>
										{arr.map((detail,index)=>(
											<AllText key={index} text={detail} />
										))}
              		</Body>
            		</CardItem>
          		</Card>
						</View>
						{btnFunc.map((detail,index)=>(
							<AllButton key={index} navigateTo={()=>this.userAction(detail[0])} buttonTitle={detail[1]} />
						))}
					</Content>
        </Container>
				

    );
  }
}

const styles = StyleSheet.create({
	container: {
	  justifyContent: 'center',
		alignItems: 'center',
		margin:15
	}
});