import React, { Component } from 'react';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, View } from 'native-base';
import { Image,Dimensions,Animated,ScrollView, RefreshControl,TouchableOpacity,StyleSheet } from 'react-native';
import AllButton from './../../common/allButton';
import AllText from '../../common/allText';
import {inputCode,qrUrl,headerUrl} from './../../config/constants';
class Home extends Component {
	
  render() {
		let code = inputCode;
		let qrcode = {uri:qrUrl};
    return ( 
			<Container>	
				<Content style={{marginTop:40}}>
					<Image source={qrcode} style={{ height:Dimensions.get('window').height/2,width: Dimensions.get('window').width}}/>
					<View style={{padding:10}}>
						<AllText text={'Input code: '+code} />
        	</View>
					<AllButton navigateTo={()=>this.props.navigation.navigate('Detail')} buttonTitle='View Leads' />
				</Content>
			</Container>
    );
  }
}

export default Home;