//initail
const initialState={
	counter:0,
}
//reducer
export default (state = initialState,action)=>{
	switch(action.type)
	{
		case 'INCREASE_COUNTER':
			return {counter:state.counter+1}
		case 'DECREASE_COUNTER':
			return {counter:state.counter-1}
		case 'CLEANUP_COUNTER':
			return {counter:0}
	}
	return state 
}