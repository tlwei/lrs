//server url
export const baseUrl = 'http://192.168.0.140:8081/example';
//user
//user pic 200 * 200
export const userData = [
    {
        name:'Ed Sheeran',
        delegate:'Delegate',
        position:'ST Engineer',
        interest:'Sleep, singing',
        userPic:'https://ichef.bbci.co.uk/images/ic/960x540/p02kq8k6.jpg',
        ticked:[
                {name:"Send Brochure",status:true},
                {name:"Send Quotation",status:false},
                {name:"Send Sales Representative",status:false},
                {name:"Send Report",status:true},
                {name:"Schedule Meeting",status:false}
            ],
        note:[
            'lorem ndaoje oaidoaie oaidmoa oiwemfdokadnf oiefmnoakenf jekf nao',
            'asd asda oaidoaie dfgd oiwemfdokadnf dfg ffg nao',
            'lorhfgem ndaoje oaidoaie sdsz oiwemfdokadnf sdf jekf gfd'
        ],
        audio:[
            'https://sample-videos.com/audio/mp3/crowd-cheering.mp3',
            'https://sample-videos.com/audio/mp3/crowd-cheering.mp3',
            'https://sample-videos.com/audio/mp3/crowd-cheering.mp3'
        ]
    },
    {
        name:'Adam Levine',
        delegate:'Player',
        position:'PC Locker',
        interest:'Yes, Boy',
        userPic:'https://i.dailymail.co.uk/1s/2018/12/08/18/7175542-6474799-image-m-34_1544293366766.jpg',
        ticked:[
            {name:"Send Brochure",status:true},
            {name:"Send Quotation",status:false},
            {name:"Send Sales Representative",status:true},
            {name:"Send Report",status:true},
            {name:"Schedule Meeting",status:false}
        ],
        note:[
            'eaads ndaoje oaidoaie oaidmoa drgdfr oiefmnoakenf jekf dfs'
        ],
        audio:[
            'https://sample-videos.com/audio/mp3/crowd-cheering.mp3',
            'https://sample-videos.com/audio/mp3/crowd-cheering.mp3',
            'https://sample-videos.com/audio/mp3/crowd-cheering.mp3'
        ]
    },
    {
        name:'Camila Cabello',
        delegate:'User',
        position:'ACT Player',
        interest:'Assess, Queen',
        userPic:'https://i.pinimg.com/originals/94/1b/4c/941b4cc68a323f6711ec3f4cedcee870.jpg',
        ticked:[
            {name:"Send Brochure",status:false},
            {name:"Send Quotation",status:false},
            {name:"Send Sales Representative",status:false},
            {name:"Send Report",status:true},
            {name:"Schedule Meeting",status:false}
        ],
        note:[
            'lorem sdfs oaidoaie oaidmoa dfgh gdr jekf nao',
            'asd ndaoje sdfa oaidmoa dfhg oiefmnoakenf dfg nao',
            'as sdfs oaidoaie oaidmoa sdf sfdrg jekf xc',
            'asd ndaoje sdfa oaidmoa dfhg oiefmnoakenf dfg nao',
            'as sdfs oaidoaie oaidmoa sdf sfdrg jekf xc'
        ],
        audio:[
            'https://sample-videos.com/audio/mp3/crowd-cheering.mp3',
            'https://sample-videos.com/audio/mp3/crowd-cheering.mp3',
            'https://sample-videos.com/audio/mp3/crowd-cheering.mp3'
        ]
    }
]
//body
export const backgroundColor = 'rgb(249, 249, 249)';
export const commonFontSize = 20;
export const listIcon = 'arrow-forward';
//title
export const mainTitle = 'LEAD Retrieval System';
//title color
export const titleColor = 'rgb(81, 81, 81)';

//btn color
export const buttonColor = 'skyblue';
//btn text
export const buttonText = 'white';

//images
// 1000 * 750
export const headerUrl = 'https://cdn.lynda.com/course/546061/546061-636330438981800263-16x9.jpg';
// 1000 * 140
export const footerUrl = 'https://cdn.lynda.com/course/546061/546061-636330438981800263-16x9.jpg';

//QRcode
export const inputCode = '123321123';
//QR uri
export const qrUrl = 'https://scanner.q.tk/images/q-tk-scanner1-300.png';
