
export const getIncrease = (action) => async dispatch => {

//action >> value get from container
  if(action == '+'){
      dispatch(_getIncrease());
  }else if(action == '-'){
      dispatch(_getDecrease());
  }else{
      dispatch(_getCleanup());
  }
};

const _getIncrease = () => ({
  //get function from reducer
  type: 'INCREASE_COUNTER',
});

const _getDecrease = () => ({
  type: 'DECREASE_COUNTER',
});

const _getCleanup = () => ({
  type: 'CLEANUP_COUNTER',
})

