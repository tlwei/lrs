import { connect } from 'react-redux';
import Profile from './../components/profile/profile';
import { getIncrease } from '../actions/homeAction';

const mapStateToProps = (state) => {
    //use container to get value from other component
	return {
		counter: state.homeReducer.counter,
		//counter >> reducer value
	};
}; 

//get function 
//could get function from home actions
const mapDispatchToProps = (dispatch, props) => ({
    increaseCounter:(action)=>{dispatch(getIncrease(action))},
	//add new function (from page component)
});
  

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
