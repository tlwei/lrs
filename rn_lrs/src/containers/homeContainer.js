import { connect } from 'react-redux';
import Home from './../components/application/home';
import { getIncrease } from '../actions/homeAction';

const mapStateToProps = (state) => {
	return {
		counter: state.homeReducer.counter,
		//counter >> reducer value
	};
};
  
const mapDispatchToProps = (dispatch, props) => ({
	increaseCounter:(action)=>{dispatch(getIncrease(action))},
	//action >> value get from page component
	//add new function (from page component)
});
  

export default connect(mapStateToProps, mapDispatchToProps)(Home);
