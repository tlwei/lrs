import React, { Component } from 'react';
import codePush from 'react-native-code-push';
import { createAppContainer } from 'react-navigation';
import { AppSwitchNavigator } from '../navigator/mainNavigator/screenNavigator'
import { YellowBox } from 'react-native';
import {Provider} from 'react-redux'
import  {store}  from './../store/index'
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

//store

class App extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}
	///code push 
	codePushStatusDidChange(status) {
		// console.log('code_push_status:',status)
		// e.g::codePush.SyncStatus.DOWNLOADING_PACKAGE
	}
	codePushDownloadDidProgress(progress) {
		// console.log('code_push_progress:',progress);
	}

	render() {
		return (
			<Provider store={store}>
				<AppContainer />
			</Provider>
		);	
	}
}

let codePushOptions = {
	checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
	mandatoryInstallMode: codePush.InstallMode.ON_NEXT_RESTART
};
App = codePush(codePushOptions)(App);

export default App;

const AppContainer = createAppContainer(AppSwitchNavigator)